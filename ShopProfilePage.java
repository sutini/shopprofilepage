import org.junit.AfterClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import java.util.*;

/**
 * Test profile page
 * @author Sutini Said 16/08/2020
 */
public class ShopProfilePage {
    @Test
    public void PageTitle() {
        /* Initiate driver instance */
        System.setProperty("webdriver.chrome.driver", "/Users/sutini/chrome/chromedriver 2");
        WebDriver driver = new ChromeDriver();
        driver.navigate().to("https://www.trustedshops.de/bewertung/info_X77B11C1B8A5ABA16DDEC0C30E7996C21.html");
        /* Check if page title exists */
        String actualTtile = driver.getTitle();
        driver.manage().window().maximize();
        String expectedTitle = "Jalousiescout.de Bewertungen | Lesen Sie 122.983 Jalousiescout.de Bewertungen";
        if (actualTtile.equalsIgnoreCase(expectedTitle))
            System.out.println("Page title exists: Test Passed.");
        else
            System.out.print("Page title is missing: Test Failed.");
        driver.close();
    }

    @Test
    public void GradeVisible() {
        /* Check if the grade is visible and is above zero */
        System.setProperty("webdriver.chrome.driver", "/Users/sutini/chrome/chromedriver 2");
        WebDriver driver = new ChromeDriver();
        driver.navigate().to("https://www.trustedshops.de/bewertung/info_X77B11C1B8A5ABA16DDEC0C30E7996C21.html");
        Boolean Display = driver.findElement(By.className("score-shop")).isDisplayed();
        System.out.println("Element displayed is :" + Display);
        WebElement element = driver.findElement(By.className("score-shop"));
        if (element.getText().equals("4.70"))
            System.out.println("Score is zero");
        else
            System.out.println("Score is above zero");
        driver.close();
    }

    @Test
    public void InfoIcon() {
        /* Hover over the info icon of the first review in the list ((i) on to right) - ensure that the popup window appears */
        System.setProperty("webdriver.chrome.driver", "/Users/sutini/chrome/chromedriver 2");
        WebDriver driver = new ChromeDriver();
        driver.navigate().to("https://www.trustedshops.de/bewertung/info_X77B11C1B8A5ABA16DDEC0C30E7996C21.html");
        Actions actions = new Actions(driver);
        WebElement element = driver.findElement(By.className("ng-star-inserted"));
        actions.moveToElement(element).perform();
        WebElement toolTip = driver.findElement(By.className("ng-star-inserted"));
        String toolTipText = toolTip.getText();
        System.out.println("toolTipText" + toolTipText);
        if (toolTipText.equalsIgnoreCase("vor 24 Tagen")) {
            System.out.println("Tooltip message did not prompt ");
        } else {
            System.out.println("Tooltip message prompted");
            driver.close();
        }
    }

    @Test
    public void StartFilter() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "/Users/sutini/chrome/chromedriver 2");
        WebDriver driver = new ChromeDriver();
        driver.navigate().to("https://www.trustedshops.de/bewertung/info_X77B11C1B8A5ABA16DDEC0C30E7996C21.html");
        driver.manage().window().maximize();
        TimeUnit.SECONDS.sleep(5);
        driver.findElement(By.xpath("//div[contains(text(), '1 Stern')]")).click();
        List<WebElement> score = driver.findElements(By.xpath("//div[contains(text(), '1 Stern')]"));
        System.out.println("Pages are filtered correctly");
        TimeUnit.SECONDS.sleep(5);
        driver.close();
        driver.quit();
    }
}
